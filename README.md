# Mojos streaming resources
 A collection of resources for streaming and streamers

## Contents:
| Name                      | Description                                                                                                                        |
| ------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| [Death's Door death screen](https://github.com/BuyMyMojo/Mojos-streaming-resources/releases/tag/DDDSv1) | a transparent video of the death screen for use as the !ded command for [wazzacrack](https://www.twitch.tv/wazzacrack) originally. |

